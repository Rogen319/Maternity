package com.chml.exception;

/**
 * Created by zzt on 16-9-30.
 */
public class FailCallException extends Exception {
    private int code;

    public FailCallException(String message, int code) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
