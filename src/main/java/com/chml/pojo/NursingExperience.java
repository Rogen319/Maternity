package com.chml.pojo;

import java.util.Date;

public class NursingExperience {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column nursing_experience.nid
     *
     * @mbggenerated
     */
    private Integer nid;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column nursing_experience.enter_date
     *
     * @mbggenerated
     */
    private Date enterDate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column nursing_experience.experience
     *
     * @mbggenerated
     */
    private Integer experience;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column nursing_experience.num_of_family
     *
     * @mbggenerated
     */
    private Integer numOfFamily;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column nursing_experience.num_of_twin
     *
     * @mbggenerated
     */
    private Integer numOfTwin;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column nursing_experience.family_type
     *
     * @mbggenerated
     */
    private String familyType;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column nursing_experience.location
     *
     * @mbggenerated
     */
    private String location;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column nursing_experience.nid
     *
     * @return the value of nursing_experience.nid
     *
     * @mbggenerated
     */
    public Integer getNid() {
        return nid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column nursing_experience.nid
     *
     * @param nid the value for nursing_experience.nid
     *
     * @mbggenerated
     */
    public void setNid(Integer nid) {
        this.nid = nid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column nursing_experience.enter_date
     *
     * @return the value of nursing_experience.enter_date
     *
     * @mbggenerated
     */
    public Date getEnterDate() {
        return enterDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column nursing_experience.enter_date
     *
     * @param enterDate the value for nursing_experience.enter_date
     *
     * @mbggenerated
     */
    public void setEnterDate(Date enterDate) {
        this.enterDate = enterDate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column nursing_experience.experience
     *
     * @return the value of nursing_experience.experience
     *
     * @mbggenerated
     */
    public Integer getExperience() {
        return experience;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column nursing_experience.experience
     *
     * @param experience the value for nursing_experience.experience
     *
     * @mbggenerated
     */
    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column nursing_experience.num_of_family
     *
     * @return the value of nursing_experience.num_of_family
     *
     * @mbggenerated
     */
    public Integer getNumOfFamily() {
        return numOfFamily;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column nursing_experience.num_of_family
     *
     * @param numOfFamily the value for nursing_experience.num_of_family
     *
     * @mbggenerated
     */
    public void setNumOfFamily(Integer numOfFamily) {
        this.numOfFamily = numOfFamily;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column nursing_experience.num_of_twin
     *
     * @return the value of nursing_experience.num_of_twin
     *
     * @mbggenerated
     */
    public Integer getNumOfTwin() {
        return numOfTwin;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column nursing_experience.num_of_twin
     *
     * @param numOfTwin the value for nursing_experience.num_of_twin
     *
     * @mbggenerated
     */
    public void setNumOfTwin(Integer numOfTwin) {
        this.numOfTwin = numOfTwin;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column nursing_experience.family_type
     *
     * @return the value of nursing_experience.family_type
     *
     * @mbggenerated
     */
    public String getFamilyType() {
        return familyType;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column nursing_experience.family_type
     *
     * @param familyType the value for nursing_experience.family_type
     *
     * @mbggenerated
     */
    public void setFamilyType(String familyType) {
        this.familyType = familyType == null ? null : familyType.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column nursing_experience.location
     *
     * @return the value of nursing_experience.location
     *
     * @mbggenerated
     */
    public String getLocation() {
        return location;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column nursing_experience.location
     *
     * @param location the value for nursing_experience.location
     *
     * @mbggenerated
     */
    public void setLocation(String location) {
        this.location = location == null ? null : location.trim();
    }
}