package com.chml.dto;

import java.util.List;

/**
 * 月嫂生活照信息格式
 */
public class WorkingPhotoInfo {
    private String url;
    private List<String> tags;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }
}
