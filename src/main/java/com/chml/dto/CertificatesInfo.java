package com.chml.dto;

import java.util.List;

/**
 * 月嫂证书信息格式
 */
public class CertificatesInfo {
    IDCardInfo id_card;
    HealthInfo health;
    List<SkillInfo> skills;
    TrainingInfo training;
    NursingInfo nursing;
    List<WorkingPhotoInfo> working_photo;

    public IDCardInfo getId_card() {
        return id_card;
    }

    public void setId_card(IDCardInfo id_card) {
        this.id_card = id_card;
    }

    public HealthInfo getHealth() {
        return health;
    }

    public void setHealth(HealthInfo health) {
        this.health = health;
    }

    public List<SkillInfo> getSkills() {
        return skills;
    }

    public void setSkills(List<SkillInfo> skills) {
        this.skills = skills;
    }

    public TrainingInfo getTraining() {
        return training;
    }

    public void setTraining(TrainingInfo training) {
        this.training = training;
    }

    public NursingInfo getNursing() {
        return nursing;
    }

    public void setNursing(NursingInfo nursing) {
        this.nursing = nursing;
    }

    public List<WorkingPhotoInfo> getWorking_photo() {
        return working_photo;
    }

    public void setWorking_photo(List<WorkingPhotoInfo> working_photo) {
        this.working_photo = working_photo;
    }
}
