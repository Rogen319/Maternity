package com.chml.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by zzt on 16-10-6.
 */
public class BaseNursingExpInfoDto {
    private int years;
    @JsonProperty(value = "family_num")
    private int familyNum;
    private List<String> exp;

    public int getYears() {
        return years;
    }

    public void setYears(int years) {
        this.years = years;
    }

    public int getFamilyNum() {
        return familyNum;
    }

    public void setFamilyNum(int familyNum) {
        this.familyNum = familyNum;
    }

    public List<String> getExp() {
        return exp;
    }

    public void setExp(List<String> exp) {
        this.exp = exp;
    }
}
