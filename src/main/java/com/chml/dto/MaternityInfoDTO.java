package com.chml.dto;

/**
 * 返回月嫂列表时的月嫂简单信息格式
 */
public class MaternityInfoDTO {
    private int id;
    private String name;
    private String head_img_url;
    private int age;
    private String birth_place;
    private int experience;
    private int level;
    private float star;
    private int evaluation_number;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHead_img_url() {
        return head_img_url;
    }

    public void setHead_img_url(String head_img_url) {
        this.head_img_url = head_img_url;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBirth_place() {
        return birth_place;
    }

    public void setBirth_place(String birth_place) {
        this.birth_place = birth_place;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public float getStar() {
        return star;
    }

    public void setStar(float star) {
        this.star = Float.parseFloat(String.format("%.1f", star));
    }

    public int getEvaluation_number() {
        return evaluation_number;
    }

    public void setEvaluation_number(int evaluation_number) {
        this.evaluation_number = evaluation_number;
    }
}
