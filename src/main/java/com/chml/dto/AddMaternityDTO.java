package com.chml.dto;

/**
 * 新增月嫂返回的数据格式
 */
public class AddMaternityDTO {
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
