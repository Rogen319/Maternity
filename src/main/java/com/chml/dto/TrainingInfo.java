package com.chml.dto;

import java.util.List;

/**
 * 月嫂培训信息格式
 */
public class TrainingInfo {
    List<String> subjects;
    List<String> hospital;

    public List<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<String> subjects) {
        this.subjects = subjects;
    }

    public List<String> getHospital() {
        return hospital;
    }

    public void setHospital(List<String> hospital) {
        this.hospital = hospital;
    }
}
