package com.chml.dto;

/**
 * 获取月嫂列表时的过滤条件格式
 */
public class MaternityFilter {
    String birth_place;
    RangeBound age;
    RangeBound experience;
    int level;
    RangeBound star;
    RangeBound evaluation_number;

    public String getBirth_place() {
        return birth_place;
    }

    public void setBirth_place(String birth_place) {
        this.birth_place = birth_place + "%";
    }

    public RangeBound getAge() {
        return age;
    }

    public void setAge(RangeBound age) {
        this.age = age;
    }

    public RangeBound getExperience() {
        return experience;
    }

    public void setExperience(RangeBound experience) {
        this.experience = experience;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public RangeBound getStar() {
        return star;
    }

    public void setStar(RangeBound star) {
        this.star = star;
    }

    public RangeBound getEvaluation_number() {
        return evaluation_number;
    }

    public void setEvaluation_number(RangeBound evaluation_number) {
        this.evaluation_number = evaluation_number;
    }
}