package com.chml.dto;
import java.util.List;
/**
 * 获取月嫂详细信息返回数据格式
 */
public class MaternityDetailDTO {
    int id;
    String name;
    int age;
    int level;
    String birth_place;
    String phone_num;
    String head_img_url;
    String intro_video_url;
    String emergencyContactPersonName;
    String emergencyContactPersonRelation;
    String emergencyContactPersonNumber;
    String marriageState;
    String procreationState;
    String educationBackground;
    List<String> language;
    GradeInfo grade;
    String certificates;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getBirth_place() {
        return birth_place;
    }

    public void setBirth_place(String birth_place) {
        this.birth_place = birth_place;
    }

    public String getPhone_num() {
        return phone_num;
    }

    public void setPhone_num(String phone_num) {
        this.phone_num = phone_num;
    }

    public String getHead_img_url() {
        return head_img_url;
    }

    public void setHead_img_url(String head_img_url) {
        this.head_img_url = head_img_url;
    }

    public String getIntro_video_url() {
        return intro_video_url;
    }

    public void setIntro_video_url(String intro_video_url) {
        this.intro_video_url = intro_video_url;
    }

    public GradeInfo getGrade() {
        return grade;
    }

    public void setGrade(GradeInfo grade) {
        this.grade = grade;
    }

    public String getCertificates() {
        return certificates;
    }

    public void setCertificates(String certificates) {
        this.certificates = certificates;
    }

    public String getEmergencyContactPersonName() {
        return emergencyContactPersonName;
    }

    public void setEmergencyContactPersonName(String emergencyContactPersonName) {
        this.emergencyContactPersonName = emergencyContactPersonName;
    }

    public String getEmergencyContactPersonRelation() {
        return emergencyContactPersonRelation;
    }

    public void setEmergencyContactPersonRelation(String emergencyContactPersonRelation) {
        this.emergencyContactPersonRelation = emergencyContactPersonRelation;
    }

    public String getEmergencyContactPersonNumber() {
        return emergencyContactPersonNumber;
    }

    public void setEmergencyContactPersonNumber(String emergencyContactPersonNumber) {
        this.emergencyContactPersonNumber = emergencyContactPersonNumber;
    }

    public String getProcreationState() {
        return procreationState;
    }

    public void setProcreationState(String procreationState) {
        this.procreationState = procreationState;
    }

    public String getEducationBackground() {
        return educationBackground;
    }

    public void setEducationBackground(String educationBackground) {
        this.educationBackground = educationBackground;
    }

    public List<String> getLanguage() {
        return language;
    }

    public void setLanguage(List<String> language) {
        this.language = language;
    }

    public String getMarriageState() {
        return marriageState;
    }

    public void setMarriageState(String marriageState) {
        this.marriageState = marriageState;
    }
}
