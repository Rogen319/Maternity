package com.chml.dto;

/**
 * 筛选条件上下界的定义类
 */
public class RangeBound{
    int min;
    int max;

    public void setMin(int min) {
        this.min = min;
    }

    public int getMin() {
        return min;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMax() {
        return max;
    }
}