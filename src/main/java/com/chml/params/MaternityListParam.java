package com.chml.params;
import com.chml.dto.MaternityFilter;
/**
 * 获取月嫂列表时的传入信息格式
 */
public class MaternityListParam {

    int uid;
    int page;
    int per_page;
    String order_by;
    boolean is_ascend = true;
    String access_token;
    MaternityFilter filter;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public MaternityFilter getFilter() {
        return filter;
    }

    public void setFilter(MaternityFilter filter) {
        this.filter = filter;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public String getOrder_by() {
        return order_by;
    }

    public void setOrder_by(String order_by) {
        this.order_by = order_by;
    }

    public boolean is_ascend() {
        return is_ascend;
    }

    public void setIs_ascend(boolean is_ascend) {
        this.is_ascend = is_ascend;
    }
}
