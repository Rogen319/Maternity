package com.chml.params;

import com.chml.dto.CertificatesInfo;
import com.chml.dto.GradeInfo;
import java.util.List;

/**
 * 添加月嫂时的传入参数格式
 */
public class AddMaternityParam {
    int uid;
    String access_token;
    String name;
    int age;
    String birth_place;
    String phone_num;
    String head_img_url;
    String intro_video_url;
    String educationBackground;
    String emergencyContactPersonName;
    String emergencyContactPersonRelation;
    String emergencyContactPersonNumber;
    String procreationState;
    String marriageState;
    List<String> language;
    int level;
    CertificatesInfo certificates;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getBirth_place() {
        return birth_place;
    }

    public void setBirth_place(String birth_place) {
        this.birth_place = birth_place;
    }

    public String getPhone_num() {
        return phone_num;
    }

    public void setPhone_num(String phone_num) {
        this.phone_num = phone_num;
    }

    public String getHead_img_url() {
        return head_img_url;
    }

    public void setHead_img_url(String head_img_url) {
        this.head_img_url = head_img_url;
    }

    public String getIntro_video_url() {
        return intro_video_url;
    }

    public void setIntro_video_url(String intro_video_url) {
        this.intro_video_url = intro_video_url;
    }

    public String getEducationBackground() {
        return educationBackground;
    }

    public void setEducationBackground(String educationBackground) {
        this.educationBackground = educationBackground;
    }

    public String getEmergencyContactPersonName() {
        return emergencyContactPersonName;
    }

    public void setEmergencyContactPersonName(String emergencyContactPersonName) {
        this.emergencyContactPersonName = emergencyContactPersonName;
    }

    public String getEmergencyContactPersonRelation() {
        return emergencyContactPersonRelation;
    }

    public void setEmergencyContactPersonRelation(String emergencyContactPersonRelation) {
        this.emergencyContactPersonRelation = emergencyContactPersonRelation;
    }

    public String getEmergencyContactPersonNumber() {
        return emergencyContactPersonNumber;
    }

    public void setEmergencyContactPersonNumber(String emergencyContactPersonNumber) {
        this.emergencyContactPersonNumber = emergencyContactPersonNumber;
    }

    public String getProcreationState() {
        return procreationState;
    }

    public void setProcreationState(String procreationState) {
        this.procreationState = procreationState;
    }

    public String getMarriageState() {
        return marriageState;
    }

    public void setMarriageState(String marriageState) {
        this.marriageState = marriageState;
    }

    public List<String> getLanguage() {
        return language;
    }

    public void setLanguage(List<String> language) {
        this.language = language;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public CertificatesInfo getCertificates() {
        return certificates;
    }

    public void setCertificates(CertificatesInfo certificates) {
        this.certificates = certificates;
    }
}
