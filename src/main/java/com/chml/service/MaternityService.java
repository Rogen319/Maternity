package com.chml.service;

import com.chml.dto.*;
import com.chml.exception.FailCallException;
import com.chml.params.AddMaternityParam;
import com.chml.params.DeleteMaternityParam;
import com.chml.params.MaternityListParam;

import java.io.IOException;
import java.util.List;

/**
 * 操作数据库月嫂信息的一些接口
 */
public interface MaternityService {
    List<MaternityInfoDTO> getMaternityList(MaternityListParam filterInfo, String name) throws Exception;

    int getMaternityListCount(MaternityListParam filterInfo, String name) throws Exception;

    int getMaternityListTotalCount() throws Exception;

    void deleteMaternity(DeleteMaternityParam idList) throws Exception;

    MaternityDetailDTO getMaternityDetail(int id, int uid, String access_token) throws Exception;

    int addMaternity(AddMaternityParam maternityInfo) throws Exception;

    int updateMaternity(int mid, AddMaternityParam maternityInfo) throws Exception;

    MaternityBaseInfo getBaseInfo(int mid) throws Exception;

    List<CertificateDto> getCertificates(int mid) throws IOException;

    List<WorkingPhotoInfo> getWorkingPhoto(int mid) throws IOException;

    List<String> getPostHospitals(int mid) throws IOException, FailCallException;
    List<String> getTrainingSubjects(int mid) throws IOException, FailCallException;
}
