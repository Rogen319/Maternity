package com.chml.service;

import com.chml.dto.BaseNursingExpInfoDto;

/**
 * Created by zzt on 16-10-6.
 */
public interface NursingService {
    BaseNursingExpInfoDto getBaseNursingExpInfoByMid(int mid);
}
