package com.chml.service.impl;

import com.chml.mapper.MediaDataMapper;
import com.chml.pojo.MediaDataExample;
import com.chml.service.MediaDataService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by zzt on 16-10-6.
 */
@Service
public class MediaDataServiceImpl implements MediaDataService {
    @Resource
    MediaDataMapper mediaDataMapper;
    @Override
    public String getIntroVideoUrl(int mid) {
        MediaDataExample example = new MediaDataExample();
        example.createCriteria().andMidEqualTo(mid).andTypeEqualTo(1);
        example.setDistinct(true);
        return mediaDataMapper.selectByExample(example).get(0).getUrl();
    }
}
