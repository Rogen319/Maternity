package com.chml.service.impl;

import com.chml.dto.BaseNursingExpInfoDto;
import com.chml.mapper.MaternityMatronBaseInfoMapper;
import com.chml.mapper.NursingExperienceMapper;
import com.chml.pojo.MaternityMatronBaseInfo;
import com.chml.pojo.NursingExperience;
import com.chml.service.MaternityService;
import com.chml.service.NursingService;
import com.chml.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by zzt on 16-10-6.
 */
@Service
public class NursingServiceImpl implements NursingService {
    @Resource
    private MaternityMatronBaseInfoMapper maternityMapper;
    @Resource
    NursingExperienceMapper nursingMapper;
    @Override
    public BaseNursingExpInfoDto getBaseNursingExpInfoByMid(int mid) {
        NursingExperience nursingExperience = nursingMapper.selectByPrimaryKey(maternityMapper.selectByPrimaryKey(mid).getNid());
        BaseNursingExpInfoDto baseNursingExpInfo = new BaseNursingExpInfoDto();
        baseNursingExpInfo.setYears(nursingExperience.getExperience());
        baseNursingExpInfo.setFamilyNum(nursingExperience.getNumOfFamily());
        List<String> exp = new ArrayList<>();
        String familyTypes = nursingExperience.getFamilyType();
        Collections.addAll(exp, familyTypes.substring(1, familyTypes.length() - 1).split(","));
        if (nursingExperience.getNumOfTwin()>0)
            exp.add("双胞胎经验");
        baseNursingExpInfo.setExp(exp);
        return baseNursingExpInfo;
    }
}
