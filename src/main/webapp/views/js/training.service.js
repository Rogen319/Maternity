/**
 * Created by zzt on 16-10-16.
 */
angular.module('services').factory('TrainingService', function ($resource, globalVar) {
    return $resource("", [],
        {
            getExamInfo: {
                method: 'POST',
                url: '../training/rest/yuesaoController/yuesaoExamPublic/:mid',
                params: {mid: '@mid'}
            },
            getPostHospitals: {
                method: 'GET',
                url: globalVar.backendUrl + '/yuesao/:mid/postHospitals',
                params: {mid: '@mid'}
            },
            getTrainingSubjects: {
                url: globalVar.backendUrl + '/yuesao/:mid/trainingSubjects',
                method: 'GET',
                params: {mid: '@mid'}
            }
        });
});