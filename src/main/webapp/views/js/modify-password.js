var myAppModule = angular.module("myApp", ["modifyPasswordController"]);
var controllerModule = angular.module("modifyPasswordController", []);
controllerModule.controller("modifyPasswordController", function ($scope,$http) {
    $scope.modifyPassword = function () {
        var old_password = $scope.old_password;
        var new_password = $scope.new_password;
        var rpassword = $scope.rpassword;
        if(new_password != rpassword){
            return;
        }
        var access_token = $.cookie("access_token");
        var uid = sessionStorage.getItem("uid");
        $http({
            method:"post",
            url: ip + "user/" + uid,
            data:{
                old_password: old_password,
                new_password: new_password,
                access_token: access_token
            }
        }).success(function(data, status, headers, config){
            alert(data.msg);
            if(data.success){
                sessionStorage.clear();
                location.href = "login.html"
            }else{
                alert(data.msg);
                if(data.msg == "登录状态已失效,请重新登录"){
                    sessionStorage.clear();
                    $.cookie("access_token", null,{path:"/"});
                    location.href = "login.html";
                }
            }
        })
    }
});