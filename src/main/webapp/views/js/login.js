var myAppModule = angular.module("myApp", ["loginController"]);
var controllerModule = angular.module("loginController", []);
controllerModule.controller("LoginController", function ($scope,$http) {
    $scope.username = $.cookie("account");
    $scope.login = function()
    {
        var account = $scope.username;
        var password = $scope.password;
        var remember = $scope.remember;
        $http({
            method:"post",
            url: ip + "login",
            data:{
                account: account,
                password: password
            }
        }).success(function(data, status, headers, config){
            if (data.success) {
                var exp = new Date();
                exp.setTime(exp.getTime() + 60 * 120 * 1000);//expires time 2hours
                $.cookie("access_token",data.User.access_token, {expires: exp, path: "/"});
                if(remember){
                    $.cookie("account", account);
                }
                sessionStorage.setItem("uid",data.User.uid);
                sessionStorage.setItem("maternity_name", data.User.name);
                location.href = "overview_page.html";
            }else{
                alert(data.msg);
            }
        })
    }
});