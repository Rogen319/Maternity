/*
 * 显示用户名字
 * */
var addMaterialImgTemplate;
var loadBody = function () {
    var username = sessionStorage.getItem("maternity_name");
    if (username == null) {
        alert("请先登录");
        location.href = "login.html";
    }
    else {
        document.getElementById("maternity_name").innerHTML = sessionStorage.getItem("maternity_name");
    }
    // 绑定上传配置
    bind_upload();
    addMaterialImgTemplate = $('div.material-img:last').prop('outerHTML');
    $.datepicker.setDefaults($.datepicker.regional['zh-CN']);
    $('.date-picker').datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true
    });
};

/*
 * 登出
 * */
var logout = function () {
    sessionStorage.clear();
    $.cookie("access_token", null,{path:"/"});
    location.href = "login.html";
};

/*
 * 加载id对应的月嫂信息
 * */
var app = angular.module('myApp', []);
app.controller('infoCtrl', function ($scope, $compile, $http) {

    //记录证书是否存在，默认不存在
    var isExist = new Array(10);
    for (var i = 0; i < 10; i++) {
        isExist[i] = false;
    }

    // 加载省市数据
    $scope.provinces = cityList;
    $scope.changeProvince = function () {
        $scope.cities = $scope.province.c;
    };

    $scope.setNoneDisplay = function () {
        document.getElementById('yyy').style.display = "none";
        document.getElementById('jzfwy').style.display = "none";
        document.getElementById('yypcy').style.display = "none";
        document.getElementById('byy').style.display = "none";
        document.getElementById('myhls').style.display = "none";
        document.getElementById('yys').style.display = "none";
        document.getElementById('hsbyz').style.display = "none";
        document.getElementById('crs').style.display = "none";
        document.getElementById('myjkzds').style.display = "none";
    };
    $scope.setAllDisplay = function () {
        if (isExist[0] == true) {
            document.getElementById('yyy').style.display = "inline-block";
        }
        if (isExist[1] == true) {
            document.getElementById('jzfwy').style.display = "inline-block";
        }
        if (isExist[2] == true) {
            document.getElementById('yypcy').style.display = "inline-block";
        }
        if (isExist[3] == true) {
            document.getElementById('byy').style.display = "inline-block";
        }
        if (isExist[4] == true) {
            document.getElementById('myhls').style.display = "inline-block";
        }
        if (isExist[5] == true) {
            document.getElementById('yys').style.display = "inline-block";
        }
        if (isExist[6] == true) {
            document.getElementById('hsbyz').style.display = "inline-block";
        }
        if (isExist[7] == true) {
            document.getElementById('crs').style.display = "inline-block";
        }
        if (isExist[9] == true) {
            document.getElementById('myjkzds').style.display = "inline-block";
        }
    };

    $scope.setNoneDisplay();
    $scope.setSelectedCertification = function (x) {
        if (x == "myhls") {
            document.getElementById('myhls').style.display = "inline-block";
        }
        else if (x == "yyy") {
            document.getElementById('yyy').style.display = "inline-block";
        }
        else if (x == "jzfwy") {
            document.getElementById('jzfwy').style.display = "inline-block";
        }
        else if (x == "yypcy") {
            document.getElementById('yypcy').style.display = "inline-block";
        }
        else if (x == "byy") {
            document.getElementById('byy').style.display = "inline-block";
        }
        else if (x == "hsbyz") {
            document.getElementById('hsbyz').style.display = "inline-block";
        }
        else if (x == "crs") {
            document.getElementById('crs').style.display = "inline-block";
        }
        else if (x == "yys") {
            document.getElementById('yys').style.display = "inline-block";
        }
        else if (x == "myjkzds") {
            document.getElementById('myjkzds').style.display = "inline-block";
        }
        else {
            $scope.setAllDisplay();
        }
    };

    var id = sessionStorage.getItem("matron_id");
    //获取uid以及access_token
    var access_token = $.cookie("access_token");
    var uid = sessionStorage.getItem("uid");
    var url = ip + "yuesao/" + id + "/detail";

    //删除证书
    $scope.deleteYyyImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("yyy_img").src = "img/upload-img.png";
            document.getElementById("yyy_img").removeAttribute('data-url');
            $scope.yyy_serial = "";
            $scope.yyy_level_select = "";
            document.getElementById('yyy').style.display = "none";
        }
    };
    $scope.deleteJzfwyImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("jzfwy_img").src = "img/upload-img.png";
            document.getElementById("jzfwy_img").removeAttribute('data-url');
            $scope.jzfwy_serial = "";
            $scope.jzfwy_level_select = "";
            document.getElementById('jzfwy').style.display = "none";
        }
    };
    $scope.deleteYypcyImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("yypcy_img").src = "img/upload-img.png";
            document.getElementById("yypcy_img").removeAttribute('data-url');
            $scope.yypcy_serial = "";
            $scope.yypcy_level_select = "";
            document.getElementById('yypcy').style.display = "none";
        }
    };
    $scope.deleteByyImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("byy_img").src = "img/upload-img.png";
            document.getElementById("byy_img").removeAttribute('data-url');
            $scope.byy_serial = "";
            $scope.byy_level_select = "";
            document.getElementById('byy').style.display = "none";
        }
    };
    $scope.deleteMyhlsImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("myhls_img").src = "img/upload-img.png";
            document.getElementById("myhls_img").removeAttribute('data-url');
            $scope.myhls_serial = "";
            document.getElementById('myhls').style.display = "none";
        }
    };
    $scope.deleteYysImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("yys_img").src = "img/upload-img.png";
            document.getElementById("yys_img").removeAttribute('data-url');
            $scope.yys_serial = "";
            document.getElementById('yys').style.display = "none";
        }
    };
    $scope.deleteHsbyzImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("hsbyz_img").src = "img/upload-img.png";
            document.getElementById("hsbyz_img").removeAttribute('data-url');
            $scope.hsbyz_serial = "";
            document.getElementById('hsbyz').style.display = "none";
        }
    };
    $scope.deleteCrsImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("crs_img").src = "img/upload-img.png";
            document.getElementById("crs_img").removeAttribute('data-url');
            $scope.crs_serial = "";
            document.getElementById('crs').style.display = "none";
        }
    };
    $scope.deleteMyjkzdsImg = function() {
        //删除确认
        var msg = "确定删除该证书吗？";
        if(confirm(msg) == true){
            document.getElementById("myjkzds_img").src = "img/upload-img.png";
            document.getElementById("myjkzds_img").removeAttribute('data-url');
            $scope.myjkzds_serial = "";
            document.getElementById('myjkzds').style.display = "none";
        }
    };

    //修改月嫂信息
    $scope.saveMatron = function () {
        //获取uid以及access_token
        var access_token = $.cookie("access_token");
        var uid = sessionStorage.getItem("uid");

        var hy;
        var sy;
        var xl = "";
        var hljy;
        var yy = [];
        var ywcpx = [];
        var ggyy = [];
        var hljt = [];

        var name = $scope.name;
        var age = $scope.age;
        var province = document.getElementById("province");
        var index = province.selectedIndex;
        var birth_place = province.options[index].value;
        var phone = $scope.phone_num;
        var emergency = $scope.emergency;
        var relation = $scope.relation;
        var contact = $scope.contact;
        var id_num = $scope.id_num;
        var health_start = $scope.health_start;
        var health_end = $scope.health_end;
        var hlsj = new Date($scope.hlsj);
        var hlsc = $scope.hlsc;//新增的护理时长
        var jtsl = $scope.jtsl;
        var health_start = new Date($scope.health_start);
        var health_end = new Date($scope.health_end);

        //婚姻状况
        var obj = document.getElementsByName("hyzk");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                hy = obj[i].nextSibling.innerText;
            }
        }

        //生育状况
        obj = document.getElementsByName("syzk");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                sy = obj[i].nextSibling.innerText;
            }
        }

        //学历状况
        obj = document.getElementsByName("xlzk");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                xl = obj[i].nextSibling.innerText;
                break;
            }
        }

        //语言能力
        obj = document.getElementsByName("yynl");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                yy.push(obj[i].nextSibling.innerText);
            }
        }

        //已完成培训
        obj = document.getElementsByName("ywcpx");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                var temp;
                if (obj[i].checked) {
                    temp = obj[i].nextSibling;
                    if (temp.nodeName == "SPAN") {
                        ywcpx.push(obj[i].nextSibling.innerText);
                    }
                    else {
                        ywcpx.push(obj[i].nextSibling.value);
                    }
                }
            }
        }

        //跟岗医院
        obj = document.getElementsByName("ggyy");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                var temp;
                if (obj[i].checked) {
                    temp = obj[i].nextSibling;
                    if (temp.nodeName == "SPAN") {
                        ggyy.push(obj[i].nextSibling.innerText);
                    }
                    else {
                        ggyy.push(obj[i].nextSibling.value);
                    }
                }
            }
        }

        //护理双胞胎经验
        obj = document.getElementsByName("hljy");
        for (var i = 0; i < obj.length; i++) {
            if (obj[i].checked) {
                hljy = obj[i].nextSibling.innerText;
            }
        }

        //护理家庭类型
        obj = document.getElementsByName("hljt");
        for (var i = 0; i < obj.length; i++) {
            var temp;
            if (obj[i].checked) {
                temp = obj[i].nextSibling;
                if (temp.nodeName == "SPAN") {
                    hljt.push(obj[i].nextSibling.innerText);
                }
                else {
                    hljt.push(obj[i].nextSibling.value);
                }
            }
        }

        var head_img_url = $('img.head-img').attr('data-url');
        var intro_video_url = $('video.intro-video').attr('data-url');
        var id_front_img_url = $('img.front_id_img').attr('data-url');
        var id_back_img_url = $('img.back_id_img').attr('data-url');
        var health_img_url = $('img.health_img').attr('data-url');
        var skills = [];
        $('.certification-img').each(function () {
            var skill_img_url = $(this).find('img.skill-img').attr('data-url');
            if (skill_img_url != null) {
                var skill = {};
                skill.img_url = skill_img_url;
                skill.name = $(this).children('.certification-name').children('span').text();
                var rank_select = $(this).find('.rank-select');
                if (rank_select != null) {
                    skill.level = parseInt(rank_select.val());
                }
                skill.sn = $(this).find('.certification-num').val();
                skills.push(skill);
            }
        });

        var working_photo = [];
        $('.material-img').each(function () {
            var url = $(this).find(".material-photo").attr('data-url');
            if (url != null) {
                var photo = {};
                photo.url = url;
                var tags = [];
                $(this).find(".tag-input").each(function () {
                    var tag = $(this).val();
                    tags.push(tag);
                });
                photo.tags = tags;
                working_photo.push(photo);
            }
        });

        // 判断必须信息是否输入
        if (age == null) {
            alert("请填写年龄");
            return;
        }

        if (name == null) {
            alert("请填写月嫂姓名");
            return;
        }

        if (phone == null) {
            alert("请填写月嫂的联系电话");
            return;
        }

        if (head_img_url == null) {
            alert("请上传头像");
            return;
        }

        if (intro_video_url == null) {
            alert("请上传介绍短片");
            return
        }

        if (emergency == null) {
            alert("请填写紧急联系人姓名");
            return;
        }

        if (relation == null) {
            alert("请填写与紧急联系人的关系");
            return;
        }

        if (contact == null) {
            alert("请填写紧急联系人的联系电话");
            return;
        }

        if ($scope.id_num == null || id_front_img_url == null || id_back_img_url == null) {
            alert("请补全身份证信息");
            return
        }

        if ($scope.health_start == null || $scope.health_end == null || health_img_url == null) {
            alert("请补全健康证信息");
            return;
        }

        if ($scope.hlsj == null) {
            alert("请选择最早护理时间");
            return;
        }

        if ($scope.city == null || $scope.province == null) {
            alert("请补全籍贯信息");
            return;
        }

        if ($scope.mLevel == null || $scope.mLevel < 1 || $scope.mLevel > 5) {
            alert("月嫂星级只能是1~5");
            return;
        }

        var birth_place = $scope.province.p + " " + $scope.city.n;

        // 判断输入合法性
        if (!/^\d{1,2}$/.test(age) || age > 80 || age < 18) {
            alert("请填写正确的年龄");
            return;
        }

        if (!/^\d{11}$/.test(phone)) {
            alert("请输入11位手机号码");
            return;
        }

        if (!/^\d{11}$/.test(contact)) {
            alert("请输入11位手机号码");
            return;
        }

        if (!/^\d{17}(\d|X)$/.test($scope.id_num)) {
            alert("请输入正确的身份证号");
            return;
        }

        if (!checkDate(health_start, health_end)) {
            alert("健康证有效期无效");
            return;
        }

        {
            var curTime = new Date().getTime();
            var hlsjTime = hlsj.getTime();
            if (curTime < hlsjTime) {
                alert("最早护理时间无效");
                return;
            }
        }

        if (!/^\d{1,2}$/.test(jtsl) || jtsl < 0 || jtsl > 99) {
            alert('护理家庭数量不合法，请输入0~99之间的数字');
            return;
        }

        if (name.length > 20) {
            alert("姓名过长，最多20字符");
            return;
        }
        if (emergency.length > 20) {
            alert("紧急联系人姓名过长，最多20字符");
            return;
        }
        if (relation.length > 20) {
            alert("与紧急联系人关系过长，最多20字符");
            return;
        }

        //通过http添加月嫂
        $http({

            method: "put",
            url: url,
            data: {
                uid: uid,
                access_token: access_token,
                name: name, // 月嫂姓名
                age: age, // 月嫂年龄
                birth_place: birth_place, // 月嫂籍贯
                phone_num: phone, // 月嫂联系电话
                head_img_url: head_img_url, // 月嫂头像地址
                intro_video_url: intro_video_url, // 月嫂介绍视频地址
                educationBackground: xl,
                emergencyContactPersonName: emergency,
                emergencyContactPersonRelation: relation,
                emergencyContactPersonNumber: contact,
                procreationState: sy,
                marriageState: hy,
                language: yy,
                level: $scope.mLevel,//页面中没有相关选项
                certificates: { // 证书资料
                    id_card: { // 身份证
                        num: $scope.id_num, // 证号
                        front_url: id_front_img_url, // 正面图片地址
                        back_url: id_back_img_url // 背面图片地址
                    },
                    health: { // 健康证
                        validity_period: { // 有效期
                            start: $scope.health_start, // 起始时间
                            end: $scope.health_end // 失效时间
                        },
                        img_url: health_img_url // 证件图片地址
                    },
                    skills: skills, // 技能证书
                    training: {
                        subjects: ywcpx, // 已完成培训项目，催乳师培训等
                        hospital: ggyy // 跟岗医院
                    },
                    nursing: {
                        first_time: $scope.hlsj, // 最早护理时间
                        experience: hlsc,//累计护理时长
                        num_of_family: jtsl, // 护理家庭数量
                        twins: hljy, // 有无护理双胞胎经验
                        family_type: // 家庭类型 双外籍、单外籍等
                        hljt,
                        location: []
                    },
                    working_photo: working_photo // 工作照链接地址
                }
            }
        }).success(function (data, status, headers, config) {
            if (data.success) {
                alert("修改成功！");
                window.close();
            }
            else {
                alert(data.msg);
                if(data.msg == "登录状态已失效,请重新登录"){
                    sessionStorage.clear();
                    $.cookie("access_token", null,{path:"/"});
                    location.href = "login.html";
                }
            }
        });
    };

    $scope.addIDImg = function () {
        document.getElementById('id1-img-input').click();
    };
    $scope.addHealthImg = function () {
        document.getElementById('health-img-input').click();
    };
    $scope.add_ywcpx = function () {
        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='ywcpx'/><input type='text' placeholder='自定义' style='width:120px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
        var template = angular.element(html);
        var dynamicYwcpxElement = $compile(template)($scope);
        var ywcpx_div = document.getElementById("ywcpx_div");
        angular.element(ywcpx_div).append(dynamicYwcpxElement);
    };

    $scope.add_ggyy = function () {
        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='ggyy'/><input type='text' placeholder='自定义' style='width:120px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
        var template = angular.element(html);
        var dynamicYwcpxElement = $compile(template)($scope);
        var ggyy_div = document.getElementById("ggyy_div");
        angular.element(ggyy_div).append(dynamicYwcpxElement);
    };

    $scope.add_hljt = function () {
        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='hljt'/><input type='text' placeholder='自定义' style='width:120px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
        var template = angular.element(html);
        var dynamicYwcpxElement = $compile(template)($scope);
        var hljt_div = document.getElementById("hljt_div");
        angular.element(hljt_div).append(dynamicYwcpxElement);
    };

    //获取月嫂信息
    $http({

        method: "post",
        url: url,
        data: {
            uid: uid,
            access_token: access_token
        }
    }).success(function (data, status, headers, config) {
        if (data.success) {

            //get certificates json array, obj[0] is json object
            var obj = eval('(' + data.data.certificates + ')');

            //设置纯文字信息
            var serial = parseInt(id);
            // alert(serial);
            document.getElementById("matron_serial").innerHTML = serial;

            $scope.name = data.data.name;
            $scope.age = data.data.age;

            //根据birth_place设定province的选中项
            var birth_place = data.data.birth_place;
            var province = birth_place.split(" ")[0];
            var city = birth_place.split(" ")[1];
            var j = 0;
            for(j=0;j<cityList.length;j++) {
                if (cityList[j].p == province) {
                    $scope.province = cityList[j];
                    $scope.cities = cityList[j].c;
                    var k = 0;
                    for (k=0;k<cityList[j].c.length;k++) {
                        if (cityList[j].c[k].n == city) {
                            $scope.city = cityList[j].c[k];
                            break
                        }
                    }
                    break;
                }
            }

            $scope.phone_num = data.data.phone_num;
            $scope.id_num = obj[0].id_card.num;
            $scope.emergency = data.data.emergencyContactPersonName;
            $scope.relation = data.data.emergencyContactPersonRelation;
            $scope.contact = data.data.emergencyContactPersonNumber;

            $scope.mLevel = data.data.level;

            switch (data.data.educationBackground) {
                case "小学":
                    document.getElementById("xl_xx").checked = true;
                    break;
                case "初中":
                    document.getElementById("xl_cz").checked = true;
                    break;
                case "中专":
                    document.getElementById("xl_zz").checked = true;
                    break;
                case "高中":
                    document.getElementById("xl_gz").checked = true;
                    break;
                case "大专":
                    document.getElementById("xl_dz").checked = true;
                    break;
            }

            if (data.data.procreationState == "已育") {
                document.getElementById("sy_yy").checked = true;
            }
            else {
                document.getElementById("sy_wy").checked = true;
            }
            if (data.data.marriageState == "已婚") {
                document.getElementById("hy_yh").checked = true;
            }
            else {
                document.getElementById("hy_wh").checked = true;
            }

            for (var i = 0; i < data.data.language.length; i++) {
                if (data.data.language[i] == "家乡话")
                    document.getElementById("jxh").checked = true;
                if (data.data.language[i] == "普通话")
                    document.getElementById("pth").checked = true;
                if (data.data.language[i] == "上海话")
                    document.getElementById("shh").checked = true;
                if (data.data.language[i] == "英语")
                    document.getElementById("english").checked = true;
            }

            var health_start = new Date(obj[0].health.validity_period.start);
            $scope.health_start = formateDate(health_start);
            var health_end = new Date(obj[0].health.validity_period.end);
            $scope.health_end = formateDate(health_end);
            var first_time_date = new Date(obj[0].nursing.first_time);
            $scope.hlsj = formateDate(first_time_date);
            $scope.jtsl = obj[0].nursing.num_of_family;
            $scope.hlsc = obj[0].nursing.experience;

            function formateDate(date) {
                var year = date.getFullYear();
                var month = pad(date.getMonth()+1,2);
                var dateOfMonth = pad(date.getDate(),2);
                return year + "-" + month + "-" + dateOfMonth;
            }

            function pad(num,n) {
                var len = num.toString().length;
                while(len < n) {
                    num = "0" + num;
                    len++;
                }
                return num;
            }

            var head_img = document.getElementById("head_img");
            head_img.src = data.data.head_img_url;
            head_img.setAttribute("data-url", data.data.head_img_url);
            var intro_video = document.getElementById("intro_video");
            intro_video.src = data.data.intro_video_url;
            intro_video.setAttribute("data-url", data.data.intro_video_url);

            var id_front_img = document.getElementById("id_front_img");
            id_front_img.src = obj[0].id_card.front_url;
            id_front_img.setAttribute("data-url", obj[0].id_card.front_url);
            var id_back_img = document.getElementById("id_back_img");
            id_back_img.src = obj[0].id_card.back_url;
            id_back_img.setAttribute("data-url", obj[0].id_card.back_url);

            var health_img = document.getElementById("health_img");
            health_img.src = obj[0].health.img_url;
            health_img.setAttribute("data-url", obj[0].health.img_url);

            //设置育婴员资格证
            var yyy_div = document.getElementById("yyy");
            var yyy_img = document.getElementById("yyy_img");

            //设置家政服务员资格证
            var jzfwy_div = document.getElementById("jzfwy");
            var jzfwy_img = document.getElementById("jzfwy_img");

            //设置营养配餐员资格证
            var yypcy_div = document.getElementById("yypcy");
            var yypcy_img = document.getElementById("yypcy_img");

            //设置保育员资格证
            var byy_div = document.getElementById("byy");
            var byy_img = document.getElementById("byy_img");

            //设置母婴护理师
            var myhls_div = document.getElementById("myhls");
            var myhls_img = document.getElementById("myhls_img");

            //设置营养师证
            var yys_div = document.getElementById("yys");
            var yys_img = document.getElementById("yys_img");

            //设置护士毕业证
            var hsbyz_div = document.getElementById("hsbyz");
            var hsbyz_img = document.getElementById("hsbyz_img");

            //设置催乳师证
            var crs_div = document.getElementById("crs");
            var crs_img = document.getElementById("crs_img");

            //设置母婴健康指导师证
            var myjkzds_div = document.getElementById("myjkzds");
            var myjkzds_img = document.getElementById("myjkzds_img");

            //alert skills info
            for (var i = 0; i < obj[0].skills.length; i++) {
                switch (obj[0].skills[i].name) {
                    case "育婴员资格证":
                        isExist[0] = true;
                        yyy_div.style.display = "inline-block";
                        yyy_img.src = obj[0].skills[i].img_url;
                        yyy_img.setAttribute('data-url', obj[0].skills[i].img_url);
                        $scope.yyy_serial = obj[0].skills[i].sn;
                        $scope.yyy_level_select = obj[0].skills[i].level.toString();
                        break;
                    case "家政服务员资格证":
                        isExist[1] = true;
                        jzfwy_div.style.display = "inline-block";
                        jzfwy_img.src = obj[0].skills[i].img_url;
                        jzfwy_img.setAttribute('data-url', obj[0].skills[i].img_url);
                        $scope.jzfwy_serial = obj[0].skills[i].sn;
                        $scope.jzfwy_level_select = obj[0].skills[i].level.toString();
                        break;
                    case "营养配餐员资格证":
                        isExist[2] = true;
                        yypcy_div.style.display = "inline-block";
                        yypcy_img.src = obj[0].skills[i].img_url;
                        yypcy_img.setAttribute('data-url', obj[0].skills[i].img_url);
                        $scope.yypcy_serial = obj[0].skills[i].sn;
                        $scope.yypcy_level_select = obj[0].skills[i].level.toString();
                        break;
                    case "保育员资格证":
                        isExist[3] = true;
                        byy_div.style.display = "inline-block";
                        byy_img.src = obj[0].skills[i].img_url;
                        byy_img.setAttribute('data-url', obj[0].skills[i].img_url);
                        $scope.byy_serial = obj[0].skills[i].sn;
                        $scope.byy_level_select = obj[0].skills[i].level.toString();
                        break;
                    case "母婴护理师":
                        isExist[4] = true;
                        myhls_div.style.display = "inline-block";
                        myhls_img.src = obj[0].skills[i].img_url;
                        myhls_img.setAttribute('data-url', obj[0].skills[i].img_url);
                        $scope.myhls_serial = obj[0].skills[i].sn;
                        break;
                    case "营养师证":
                        isExist[5] = true;
                        yys_div.style.display = "inline-block";
                        yys_img.src = obj[0].skills[i].img_url;
                        yys_img.setAttribute('data-url', obj[0].skills[i].img_url);
                        $scope.yys_serial = obj[0].skills[i].sn;
                        break;
                    case "护士毕业证":
                        isExist[6] = true;
                        hsbyz_div.style.display = "inline-block";
                        hsbyz_img.src = obj[0].skills[i].img_url;
                        hsbyz_img.setAttribute('data-url', obj[0].skills[i].img_url);
                        $scope.hsbyz_serial = obj[0].skills[i].sn;
                        break;
                    case "催乳师证":
                        isExist[7] = true;
                        crs_div.style.display = "inline-block";
                        crs_img.src = obj[0].skills[i].img_url;
                        crs_img.setAttribute('data-url', obj[0].skills[i].img_url);
                        $scope.crs_serial = obj[0].skills[i].sn;
                        break;
                    case "母婴健康指导师证":
                        isExist[9] = true;
                        myjkzds_div.style.display = "inline-block";
                        myjkzds_img.src = obj[0].skills[i].img_url;
                        myjkzds_img.setAttribute('data-url', obj[0].skills[i].img_url);
                        $scope.myjkzds_serial = obj[0].skills[i].sn;
                        break;
                }
            }

            // alert training subjects and hospitals info
            for (var i = 0; i < obj[0].training.subjects.length; i++) {
                switch (obj[0].training.subjects[i]) {
                    case "母乳喂养培训":
                        document.getElementById("mrwypx").checked = true;
                        break;
                    case "小儿推拿培训":
                        document.getElementById("xrtnpx").checked = true;
                        break;
                    case "产后护理培训":
                        document.getElementById("chhlpx").checked = true;
                        break;
                    case "":
                        break;
                    default:
                        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='ywcpx'/><input type='text' value='" + obj[0].training.subjects[i] + "' style='width:120px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
                        var template = angular.element(html);
                        var dynamicYwcpxElement = $compile(template)($scope);
                        var ywcpx_div = document.getElementById("ywcpx_div");
                        angular.element(ywcpx_div).append(dynamicYwcpxElement);
                        break;
                }
            }
            for (var i = 0; i < obj[0].training.hospital.length; i++) {
                switch (obj[0].training.hospital[i]) {
                    case "一妇婴":
                        document.getElementById("fy").checked = true;
                        break;
                    case "红房子":
                        document.getElementById("hfz").checked = true;
                        break;
                    case "国妇婴":
                        document.getElementById("gfy").checked = true;
                        break;
                    case "":
                        break;
                    default:
                        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='ggyy'/><input type='text' value='" + obj[0].training.hospital[i] + "' style='width:120px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
                        var template = angular.element(html);
                        var dynamicYwcpxElement = $compile(template)($scope);
                        var ggyy_div = document.getElementById("ggyy_div");
                        angular.element(ggyy_div).append(dynamicYwcpxElement);
                        break;
                }
            }

            if (obj[0].nursing.num_of_twins == 0) {
                document.getElementById("hl_w").checked = true;
            }
            else {
                document.getElementById("hl_y").checked = true;
            }

            for (var i = 0; i < obj[0].nursing.family_type.length; i++) {
                switch (obj[0].nursing.family_type[i]) {
                    case "双外籍人士":
                        document.getElementById("swjrs").checked = true;
                        break;
                    case "单外籍人士":
                        document.getElementById("dwjrs").checked = true;
                        break;
                    case "多月嫂协作":
                        document.getElementById("dysxz").checked = true;
                        break;
                    case "无母亲陪护":
                        document.getElementById("wmqpx").checked = true;
                        break;
                    case "":
                        break;
                    default:
                        var html = "<div class='checkbox-item'> <label> <input type='checkbox' checked='checked' name='hljt'/><input type='text' value='" + obj[0].nursing.family_type[i] + "' style='width:120px;border:none;outline:medium;color:#aaaaaa;'></label> </div>";
                        var template = angular.element(html);
                        var dynamicYwcpxElement = $compile(template)($scope);
                        var hljt_div = document.getElementById("hljt_div");
                        angular.element(hljt_div).append(dynamicYwcpxElement);
                        break;
                }
            }

            // //alert working_phote info
            for (var i = 0; i < obj[0].working_photo.length; i++) {
                var html = "<div class='material-img' style='margin-top:15px;width:260px;height:177px;'>" +
                    "<div class='certification-name' style='visibility:hidden;'><span>名称</span></div>" +
                    "<a class='upload_media' href='javascript:;' data-type='img'>" +
                    "<img class='material-photo' src='" + obj[0].working_photo[i].url + "' data-url='" + obj[0].working_photo[i].url + "'>" +
                    "</a>" +
                    "<input type='file' accept='image/*' style='display: none'>" +
                    "<div class='tag-div'>" +
                    "<div class='tag-item'><input class='tag-input tag-input1' type='text' value='" + obj[0].working_photo[i].tags[0] + "'></div>" +
                    "<div class='tag-item'><input class='tag-input tag-input2' type='text' value='" + obj[0].working_photo[i].tags[1] + "'></div>" +
                    "<div class='tag-item'><input class='tag-input tag-input3' type='text' value='" + obj[0].working_photo[i].tags[2] + "'></div>" +
                    "<div class='tag-item'><input class='tag-input tag-input4' type='text' value='" + obj[0].working_photo[i].tags[3] + "'></div>" +
                    "</div>" +
                    "</div>";
                var template = angular.element(html);
                var dynamicYwcpxElement = $compile(template)($scope);
                var work_photo_div = document.getElementById("work_photo_div");
                angular.element(work_photo_div).append(dynamicYwcpxElement);
            }
        }
        else {
            alert(data.msg);
            if(data.msg == "登录状态已失效,请重新登录"){
                sessionStorage.clear();
                $.cookie("access_token", null,{path:"/"});
                location.href = "login.html";
            }
        }
    })
    ;

});
//设置婚姻状况为单选
function hy_yh_select() {
    if (document.getElementById("hy_wh").checked) {
        document.getElementById("hy_wh").checked = false;
    }
}
function hy_wh_select() {
    if (document.getElementById("hy_yh").checked) {
        document.getElementById("hy_yh").checked = false;
    }
}

//设置生育为单选
function sy_yy_select() {
    if (document.getElementById("sy_wy").checked) {
        document.getElementById("sy_wy").checked = false;
    }
}
function sy_wy_select() {
    if (document.getElementById("sy_yy").checked) {
        document.getElementById("sy_yy").checked = false;
    }
}

//设置学历状况为单选
function xl_xx_select() {
    if (document.getElementById("xl_cz").checked) {
        document.getElementById("xl_cz").checked = false;
    }
    if (document.getElementById("xl_zz").checked) {
        document.getElementById("xl_zz").checked = false;
    }
    if (document.getElementById("xl_gz").checked) {
        document.getElementById("xl_gz").checked = false;
    }
    if (document.getElementById("xl_dz").checked) {
        document.getElementById("xl_dz").checked = false;
    }
}
function xl_cz_select() {
    if (document.getElementById("xl_xx").checked) {
        document.getElementById("xl_xx").checked = false;
    }
    if (document.getElementById("xl_zz").checked) {
        document.getElementById("xl_zz").checked = false;
    }
    if (document.getElementById("xl_gz").checked) {
        document.getElementById("xl_gz").checked = false;
    }
    if (document.getElementById("xl_dz").checked) {
        document.getElementById("xl_dz").checked = false;
    }
}
function xl_zz_select() {
    if (document.getElementById("xl_xx").checked) {
        document.getElementById("xl_xx").checked = false;
    }
    if (document.getElementById("xl_cz").checked) {
        document.getElementById("xl_cz").checked = false;
    }
    if (document.getElementById("xl_gz").checked) {
        document.getElementById("xl_gz").checked = false;
    }
    if (document.getElementById("xl_dz").checked) {
        document.getElementById("xl_dz").checked = false;
    }
}
function xl_gz_select() {
    if (document.getElementById("xl_xx").checked) {
        document.getElementById("xl_xx").checked = false;
    }
    if (document.getElementById("xl_cz").checked) {
        document.getElementById("xl_cz").checked = false;
    }
    if (document.getElementById("xl_zz").checked) {
        document.getElementById("xl_zz").checked = false;
    }
    if (document.getElementById("xl_dz").checked) {
        document.getElementById("xl_dz").checked = false;
    }
}
function xl_dz_select() {
    if (document.getElementById("xl_xx").checked) {
        document.getElementById("xl_xx").checked = false;
    }
    if (document.getElementById("xl_cz").checked) {
        document.getElementById("xl_cz").checked = false;
    }
    if (document.getElementById("xl_zz").checked) {
        document.getElementById("xl_zz").checked = false;
    }
    if (document.getElementById("xl_gz").checked) {
        document.getElementById("xl_gz").checked = false;
    }
}

//设置护理经验为单选
function hl_y_select() {
    if (document.getElementById("hl_w").checked) {
        document.getElementById("hl_w").checked = false;
    }
}
function hl_w_select() {
    if (document.getElementById("hl_y").checked) {
        document.getElementById("hl_y").checked = false;
    }
}

function addMaterialImg() {
    $('.material-photo').parent().removeAttr('data-on-uploaded');
    $('div.material-img:last').parent().append(addMaterialImgTemplate);
    bind_upload();
}

function checkDate(start, end) {
    var startTime = start.getTime();
    var endTime = end.getTime();
    var curTime = new Date().getTime();
    return startTime <= curTime && curTime <= endTime;
}