/**
 * Created by zzt on 16-10-6.
 */
angular.module('services').factory('UserService',function ($resource, globalVar, $cookies) {
    return $resource("", {},
        {
            getCurrentUserBaseInfo: {
                url: globalVar.backendUrl + "/user/base",
                headers: {
                    access_token: $cookies.get("access_token")
                }
            }
        });
})