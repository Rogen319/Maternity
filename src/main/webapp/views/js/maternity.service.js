/**
 * Created by zzt on 16-10-4.
 */
'use strict';
angular.module('services').factory('MaternityService', function ($resource, globalVar, $cookies) {
    return $resource("", {},
        {
            getBaseInfo: {
                method: 'GET',
                url: globalVar.backendUrl + "/yuesao/:mid/base",
                params: {mid: '@mid'}
            },
            getGradePoints: {
                method: 'GET',
                url: globalVar.backendUrl + "/yuesao/:mid/grade/points"
            },
            getCertificates: {
                url: globalVar.backendUrl + "/yuesao/:mid/certificates"
            },
            getNursingExp:{
                url: globalVar.backendUrl + "/yuesao/:mid/nursing/base"
            },
            getWorkingPhoto:{
                url:globalVar.backendUrl + "/yuesao/:mid/photo/working"
            }
        });
});